# AMQ SSL

https://developers.redhat.com/blog/2020/08/26/connecting-external-clients-to-red-hat-amq-broker-on-red-hat-openshift#part_1__generating_credentials_for_tls_connections

## Create TLS Keystore and Truststore
https://github.com/garethahealy/amq-jaas-cert/blob/master/generated-certs/HOW-TO.md

```
export CLIENT_KEYSTORE_PASSWORD=password
export CLIENT_TRUSTSTORE_PASSWORD=password
export BROKER_KEYSTORE_PASSWORD=password
export BROKER_TRUSTSTORE_PASSWORD=password 

#Client Keystore
keytool -genkey -alias client -keyalg RSA -keystore client.ks -storepass $CLIENT_KEYSTORE_PASSWORD -keypass $CLIENT_KEYSTORE_PASSWORD \
-dname "CN=client-amqtest-amq-tcp-ssl, O=MO, C=UK"

#Server Keystore
keytool -genkey -alias broker -keyalg RSA -keystore broker.ks -storepass $CLIENT_KEYSTORE_PASSWORD -keypass $CLIENT_KEYSTORE_PASSWORD \
-dname "CN=*.apps.cmp-nonprod.theosmo.com, O=Motability Operations Limited, L=London, C=GB"

#Export Client PublicKey
keytool -export -alias client -keystore client.ks -storepass $CLIENT_KEYSTORE_PASSWORD -file client.cert

#Export Server PublicKey
keytool -export -alias broker -keystore broker.ks -storepass $BROKER_KEYSTORE_PASSWORD -file broker.cert 

#Import Server PublicKey into Client Truststore
keytool -import -alias broker -keystore client.ts -file broker.cert -storepass $CLIENT_TRUSTSTORE_PASSWORD -trustcacerts -noprompt

#Import Client PublicKey into Server Truststore
keytool -import -alias client -keystore broker.ts -file client.cert -storepass $BROKER_TRUSTSTORE_PASSWORD -trustcacerts -noprompt

#Import Server PublicKey into Server Truststore (i.e.: trusts its self)
keytool -import -alias broker -keystore broker.ts -file broker.cert -storepass $BROKER_TRUSTSTORE_PASSWORD -trustcacerts -noprompt


```

## Create Secret for broker keystore and truststore

```
$ oc create secret generic amq-tls-secret \
--from-file=broker.ks \
--from-literal=keyStorePassword=password \
--from-file=broker.ts \
--from-literal=trustStorePassword=password

```

## Deploy AMQ with SSL exposed

In a namespace called __fuse-pilot__ in OCP3, deploy distinct brokers for each environment, with TCP SSL and a route for each one:
- fuse-amq-dev
- fuse-amq-e2e
- fuse-amq-uat

```
oc new-app amq63-ssl -p APPLICATION_NAME=fuse-amq-dev -p MQ_QUEUES=test -p MQ_TOPICS=test -p MQ_USERNAME=userJpd \
-p MQ_PASSWORD=TebrmU8a -p AMQ_SECRET=amq-tls-secret -p AMQ_TRUSTSTORE=broker.ts -p AMQ_TRUSTSTORE_PASSWORD=password \
-p AMQ_KEYSTORE=broker.ks -p AMQ_KEYSTORE_PASSWORD=password -n fuse-pilot
```

## Deploy PostgreSQL in OCP4

In each environment in OCP4, deploy a single PostgreSQL pod
- alfa-dev environment
- alfa-e2e environment
- alfa-uat environment

```
oc new-app postgresql-ephemeral -p POSTGRESQL_USER=postgre -p POSTGRESQL_PASSWORD=postgre -n alfa-dev

oc new-app postgresql-ephemeral -p POSTGRESQL_USER=postgre -p POSTGRESQL_PASSWORD=postgre -n alfa-e2e

```

## Create items in 1password vaults, 

Create items in 1password vaults, to point to each broker and local database

```
```

## Connect client

```
Usage: producer [OPTIONS]
Description: Demo producer that can be used to send messages to the broker
Options :
    [--brokerUrl                                   URL] - connection factory url; default ActiveMQConnectionFactory.DEFAULT_BROKER_URL
    [--user                                         ..] - connection user name
    [--password                                     ..] - connection password
    [--destination               queue://..|topic://..] - producer destination; default queue://TEST
    [--persistent                           true|false] - use persistent or non persistent messages; default true
    [--messageCount                                  N] - number of messages to send; default 1000
    [--sleep                                         N] - millisecond sleep period between sends or receives; default 0
    [--transactionBatchSize                          N] - use send transaction batches of size N; default 0, no jms transactions
    [--parallelThreads                               N] - number of threads to run in parallel; default 1
    [--msgTTL                                        N] - message TTL in milliseconds
    [--messageSize                                   N] - size in bytes of a BytesMessage; default 0, a simple TextMessage is used
    [--textMessageSize                               N] - size in bytes of a TextMessage, a Lorem ipsum demo TextMessage is used
    [--message                                      ..] - a text string to use as the message body
    [--payloadUrl                                  URL] - a url pointing to a document to use as the message body
    [--msgGroupID                                   ..] - JMS message group identifier

./activemq producer \
    -Djavax.net.ssl.trustStore=/Users/jtavares/Work/Projects/Motability/amq-ssl/client.ts \
    -Djavax.net.ssl.trustStorePassword=password \
    -Djavax.net.ssl.keyStore=/Users/jtavares/Work/Projects/Motability/amq-ssl/client.ks \
    -Djavax.net.ssl.keyStorePassword=password \
    --brokerUrl ssl://amq-tcp-ssl-exemplar-dev.apps.cmp-nonprod.theosmo.com:443 \
    --user userJpd \
    --password TebrmU8a \
    --destination queue://test \
    --messageCount 1 \
    --message HelloWorld

./activemq producer \
    -Djavax.net.ssl.trustStore=/Users/jtavares/Work/Projects/Motability/amq-ssl/client.ts \
    -Djavax.net.ssl.trustStorePassword=password \
    -Djavax.net.ssl.keyStore=/Users/jtavares/Work/Projects/Motability/amq-ssl/client.ks \
    -Djavax.net.ssl.keyStorePassword=password \
    --brokerUrl ssl://amq-tcp-ssl-joao-sandbox.apps.k6w8mqhd.uksouth.aroapp.io:443 \
    --user userJpd \
    --password TebrmU8a \
    --destination queue://test \
    --messageCount 50 \
    --message HelloWorld


ISSUE: Handshake was failing: append to client JAVA_OPTS: -Djavax.net.debug=ssl,handshake
SOLVED: the broker certificate CN must have the wildcard hostname for OCP - CN=*.apps.cmp-nonprod.theosmo.com
```

How to get the cert from a url:
```
echo | openssl s_client -servername amq-tcp-ssl-exemplar-dev.apps.cmp-nonprod.theosmo.com -connect https://amq-tcp-ssl-exemplar-dev.apps.cmp-nonprod.theosmo.com:443 |\
  sed -ne '/-BEGIN CERTIFICATE-/,/-END CERTIFICATE-/p' > certificate.crt

openssl s_client -connect amq-tcp-ssl-exemplar-dev.apps.cmp-nonprod.theosmo.com:443 -showcerts > broker.crt

keytool -import -alias broker-ssl -keystore client.ts -file broker.crt -storepass $CLIENT_TRUSTSTORE_PASSWORD -trustcacerts -noprompt

keytool -import -alias broker -keystore client.ts -file cnbroker-amqtest-amq-tcp-ssl-chain.pem -storepass $CLIENT_TRUSTSTORE_PASSWORD -trustcacerts -noprompt
```